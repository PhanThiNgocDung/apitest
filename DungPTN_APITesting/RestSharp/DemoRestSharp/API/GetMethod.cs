﻿using DemoRestSharp.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRestSharp.API
{
    public class GetMethod
    {
        /// <summary>
        /// Create list container all user from API
        /// </summary>
        /// <returns>List<user></returns>
        public static List<User> GetAllUser()
        {
            RestClient client = new RestClient("https://jsonplaceholder.typicode.com");
            RestRequest request = new RestRequest("users", Method.GET);

            request.AddHeader("Accept", "aplication/json");
            request.RequestFormat = DataFormat.Json;

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            List<User> users = JsonConvert.DeserializeObject<List<User>>(content);
            return users;
        }

        /// <summary>
        /// Create list container user by ID from API
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<User> GetUserID(string id)
        {

            RestClient client = new RestClient("https://jsonplaceholder.typicode.com/");
            RestRequest request = new RestRequest($"users/?id={id}", Method.GET);
            request.RequestFormat = DataFormat.Json;

            IRestResponse restResponse = client.Execute(request);
            var content = restResponse.Content;

            List<User> users = JsonConvert.DeserializeObject<List<User>>(content);
            return users;
        }

    }
}
