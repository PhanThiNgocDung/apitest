﻿using DemoRestSharp.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRestSharp.Helper
{
    public class APIHelper<T>
    {
        /// <summary>
        /// Create variable baseUrl 
        /// </summary>
        string baseUrl = "https://jsonplaceholder.typicode.com";

        /// <summary>
        /// Create RestClient 
        /// </summary>
        /// <returns>RestClient</returns>
        public RestClient Client()
        {
            RestClient client = new RestClient(baseUrl);
            return client;
        }

        /// <summary>
        /// Create RestRequest
        /// </summary>
        /// <param name="path"></param>
        /// <param name="method"></param>
        /// <returns>RestRequest</returns>
        public RestRequest Request(string path, Method method)
        {
            switch (path)
            {
                case "/users":
                    return new RestRequest(baseUrl + path, method);
                case "/todos":
                    return new RestRequest(baseUrl + path, method);
                case "/posts":
                    return new RestRequest(baseUrl + path, method);
                default:
                    return new RestRequest(baseUrl + path, method);
                   
            }
        }

        /// <summary>
        /// Create RestRequest
        /// </summary>
        /// <param name="path"></param>
        /// <param name="method"></param>
        /// <param name="post"></param>
        /// <returns>RestRequest</returns>
        public RestRequest RequestPost(string path, Method method,PostBody post)
        {
            RestRequest request = new RestRequest(baseUrl + path, method);
            request.AddJsonBody(post);
            return request;
        }
        /// <summary>
        /// Create RestRequest for method get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RestRequest RequestUserByID(string id)
        {
            RestRequest request = new RestRequest(baseUrl+$"/users/?id={id}", Method.GET);
            //request.RequestFormat = DataFormat.Json;
            return request;
        }


        /// <summary>
        /// Create IRestResponse impliment RestRequest
        /// </summary>
        /// <param name="client"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public IRestResponse GetResponse(RestClient client, RestRequest request )
        {
            return client.Execute(request);
        }

        /// <summary>
        /// Get content list from  API
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public List<T> GetContentList(IRestResponse response)
        {
            var content = response.Content;
            var obj = JsonConvert.DeserializeObject<List<T>>(content);
            return obj;
        }

        /// <summary>
        /// Get content object from API
        /// </summary>
        /// <typeparam name="Model"></typeparam>
        /// <param name="response"></param>
        /// <returns></returns>
        public Model GetContent<Model>(IRestResponse response)
        {
            var content = response.Content;
            var obj = JsonConvert.DeserializeObject<Model>(content);
            return obj;
        }
    }
}
