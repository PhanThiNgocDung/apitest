﻿using DemoRestSharp.Helper;
using DemoRestSharp.Model;
using FluentAssertions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Xunit;

namespace TestAPI
{
    public class Demo
    {
        [Trait("GETS", "")]
        [Fact(DisplayName = "All user count ID=10")]
        public void GetAllUser()
        {
            var users = new APIHelper<User>();
            RestClient client = users.Client();
            RestRequest request = users.Request("/users", Method.GET);

            IRestResponse response = users.GetResponse(client, request);
            List<User> content = users.GetContentList(response);
            content.Count.Should().Be(10);
            int statusCode = (int)response.StatusCode;
            statusCode.Should().Be((int)HttpStatusCode.OK);
        }
    }
}
