﻿using DemoRestSharp.Helper;
using DemoRestSharp.Model;
using FluentAssertions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Xunit;

namespace TestAPI
{
    public class APITest
    {
        [Trait("GET", "")]
        [Fact(DisplayName = "All user count ID=10")]
        public void GetAllUser()
        {
            var users = new APIHelper<User>();
            RestClient client = users.Client();
            RestRequest request = users.Request("/users", Method.GET);

            IRestResponse response = users.GetResponse(client, request);
            List<User> content = users.GetContentList(response);
            content.Count.Should().Be(10);
            int statusCode = (int)response.StatusCode;
            statusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Trait("GET","")]
        [Fact(DisplayName = "User id = 2")]
        public void GetUserID()
        {
            //Action
            var users = new APIHelper<User>();
            RestClient client = users.Client();
            RestRequest request = users.RequestUserByID("2");
            IRestResponse response = users.GetResponse(client, request);
            List<User> content = users.GetContentList(response);

            //Assert
            content[0].Id.Should().Be(2,"Id isn't 2");
            content[0].Name.Should().Be("Ervin Howell", "Name isn't Ervin Howell");
            content[0].Address.Street.Should().Be("Victor Plains", "Street isn't Victor Plains");
            response.Request.Timeout.Should().BeLessThan(1000);
            int statusCode = (int)response.StatusCode;
            statusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Trait("POST", "")]
        [Fact(DisplayName = "Create response post")]
        public void ShouldBePostUser()
        {
            //Action
            var post = new APIHelper<PostBody>();
            RestClient client = post.Client();
            var item = new PostBody
            {
                UserId = "11",
                Title = "sunt aut faceret",
                Body = "test"
            };
            RestRequest request = post.RequestPost("/posts", Method.POST, item);
            IRestResponse response = post.GetResponse(client, request);
            PostBody content = post.GetContent<PostBody>(response);
            //Assert
            content.UserId.Should().Be("11", "UserId isn't 11");
            content.Title.Should().Be("sunt aut faceret", "Title isn't sunt aut faceret");
            content.Body.Should().Be("test", "Body isn't test");
            content.Id.Should().Be(101);

            response.Request.Timeout.Should().BeLessThan(1000);

            int status = Convert.ToInt32(response.StatusCode);
            status.Should().Be((int)HttpStatusCode.Created,"StatusCode isn't 201");



        }
    }
}
