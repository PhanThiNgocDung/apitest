﻿using DemoRestSharp.API;
using DemoRestSharp.Helper;
using DemoRestSharp.Model;
using FluentAssertions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;



namespace API_Testing
{
    public class TestAPI
    {
        [Trait("GET","")]
        [Fact]
        public void GetCountUserShouldBe10()
        {
            var users = new APIHelper<User>();
            RestClient client = users.Client();
            RestRequest request = users.Request("/users", Method.GET);
            
            IRestResponse response = users.GetResponse(client, request);
            List<User> content = users.GetContentList(response);
            Assert.Equal(10, content.Count);
            Assert.True(response.StatusCode == HttpStatusCode.OK);
            Assert.True(response.Request.Timeout < 1000);
        }

        [Trait("GET", "")]
        [Fact(DisplayName = "GET user id =2, validate ID in response body should be 2, name should be \"Ervin Howell\", address / stress should Victor Plains")]
        public void ShouldVerifyGetUserID()
        {
            var users = new APIHelper<User>();
            RestClient client = users.Client();
            RestRequest request = users.RequestUserByID("2");

            IRestResponse response = users.GetResponse(client, request);
            List<User> content = users.GetContentList(response);

            Assert.Equal(2, content[0].Id);
            Assert.Equal("Ervin Howell", content[0].Name);
            Assert.Equal("Victor Plains", content[0].Address.Street);
            Assert.True(response.StatusCode == HttpStatusCode.OK);
            Assert.True(response.Request.Timeout < 1000);
        }

        [Trait("POST", "")]
        [Fact(DisplayName = "")]
        public void ShouldBePostUser()
        {
            var post = new APIHelper<PostBody>();
            RestClient client = post.Client();
            var item = new PostBody
            {
                UserId = "11",
                Title = "sunt aut faceret",
                Body = "test"
            };
            RestRequest request = post.RequestPost("/posts", Method.POST,item);
            IRestResponse response = post.GetResponse(client, request);
            PostBody content = post.GetContent<PostBody>(response);
            //Assert
            Assert.Equal("11", content.UserId);
            Assert.Equal("sunt aut faceret", content.Title);
            Assert.Equal("test", content.Body);
            Assert.Equal(101, content.Id);
            Assert.True(response.StatusCode == HttpStatusCode.Created);
            Assert.True(response.Request.Timeout < 1000);

        }
    }
}
